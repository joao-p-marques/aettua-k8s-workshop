FROM node:lts-alpine
EXPOSE 8080
COPY nodejs/dist/server.js .
CMD node server.js
